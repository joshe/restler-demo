FROM trafex/alpine-nginx-php7

USER root
RUN apk add --no-cache php7-tokenizer mysql-client
USER nobody

